//- file : HWProxy.h


#ifndef __HWPROXY_H__
#define __HWPROXY_H__

#include <tango.h>
#include <yat/threading/Task.h>
#include <yat/time/Timer.h>


namespace HWProxy_ns
{
  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  const size_t OFF_MSG               = yat::FIRST_USER_MSG + 100;
  const size_t ON_MSG                = yat::FIRST_USER_MSG + 101;
  const size_t RESET_INTERLOCKS_MSG  = yat::FIRST_USER_MSG + 102;
  const size_t WRITE_CURRENT_MSG     = yat::FIRST_USER_MSG + 103;
  const size_t REMOTE_MSG            = yat::FIRST_USER_MSG + 107;
  const size_t LOCAL_MSG             = yat::FIRST_USER_MSG + 108;

  //- startup and communication errors
  typedef enum
  {
    PS_UNKNOWN_ERROR = 0,
    PS_INITIALIZATION_ERROR,
    PS_DEVICE_PROXY_ERROR,
    PS_COMMUNICATION_ERROR,
    PS_HARDWARE_ERROR,
    PS_NO_ERROR,
  } ComState;

  //- the list of commands for polling
  typedef enum 
  {
    CMD_RD_PS_STATUS = 0,
    CMD_RD_CURRENT,
    CMD_RD_VOLTAGE,
    CMD_RD_SETPOINT,
    CMD_MAX_INDEX
  }PolledCmdsIndex;  
 
  static const std::string polled_cmds_str[CMD_MAX_INDEX + 1] =
  {
    "S1H\r",
    "AD 8\r",
    "AD 2\r",
    "AD 16\r",
    "ERRC\r"
  };

//------------------------------------------------------------------------
//- the AbstractClass definition
//------------------------------------------------------------------------
  class HWProxy : public yat::Task, public Tango::LogAdapter
  {

    public : 
    //- create a dedicated type for HWProxy configuration
    //---------------------------------------------------------
    typedef struct Config
    {
      //- members --------------------
      std::string url;
      size_t periodic_timeout_ms;
      double ratio;

      //- ctor -----------------------
      Config ();
      Config (const Config & src);

      //- operator= ------------------
      void operator= (const Config& src);
    } Config;

    //- Ctor
    HWProxy (Tango::DeviceImpl * _host_device,
             const Config & conf);
    //- Dtor
    virtual ~HWProxy (void);

    //- APD accessors and mutator
    double get_current (void) {
      return m_current;
    }
    double get_voltage (void) {
      return m_voltage;
    }
    double get_setpoint (void) {
      return m_setpoint;
    }
    bool get_is_on (void)
    {
      return ((m_ps_state & 0x800) != 0)? true : false;
    }
    bool get_is_fault (void)
    {
      return ((m_ps_state & 0x4000) != 0)? true : false;
    }
    bool get_is_analog (void)
    {
      return false;
    }
    unsigned long get_ps_state (void)
    {
      std::cout << " get_ps_state : returning value " << std::hex << m_ps_state << std::endl;
      return m_ps_state;
    }

    ComState get_com_state (void)
    {
      return m_com_state;
    }

    //- State and Status accessors
    Tango::DevState get_state (void);

    const std::string & get_status (void);


  private :
	  //- handle_message -----------------------
	  virtual void handle_message (yat::Message& msg)
      throw (yat::Exception);

    //- here goes the code for communication with the HW
    void read_hard (void);

    //- read value on the Hard, returns a double value
    double get_process_value_d (const std::string & cmd);
    long get_process_value_l (const std::string & cmd);

    //- read value on the Hard, returns the string value
    void get_string (const std::string & cmd, std::string & resp);
    //- just sends a command (the controller does not respond)
    void send_command (const std::string & cmd);

    void get_process_state (void);
    //- the PS state : general PS state
    ComState m_com_state;

    //- the host device 
    Tango::DeviceImpl * host_dev;

    //- Configuration
    Config conf;

    //- these data are accessed in derivate classes : protected

    //- voltage
    double m_voltage;
    //- the current
    double m_current;
    //- the setpoint
    double m_setpoint;
    //- the state of ps (numerical value)
    unsigned long m_ps_state;

    //- the HW status populated by derivate class
    std::string m_ps_status;

    //- the model name of the instrument
    std::string m_identity;

    //- Tango communication device stuff
    Tango::DeviceProxy * m_comm_dev;

   
    //- the status text
    std::string m_status;
    std::string last_error;

    int m_cmd_index;

    bool m_local;
  }; 

} //- namespace

#endif // __HWPROXY_H__
