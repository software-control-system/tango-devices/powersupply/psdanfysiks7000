//- file : HWProxy.cpp
//- test for inheritage scheme
//- this is the mother class


#include <math.h>
#include <yat/utils/XString.h>
#include "HWProxy.h"
#include <iomanip>



// ============================================================================
// some defines enums and constants
// ============================================================================
static const double __NAN__ = ::sqrt(-1.);
//- non standard VC libraries
#if (_MSC_VER)
#include <cfloat>
#define isnan _isnan
#endif


static const std::string hw_state_str[] =
{
  "Unknown Error",
  "Initialisation Error",
  "Device Proxy Error",
  "Communication Error",
  "Hardware Error",
  "Up and Running"
};

//--------------------------------------------------------------------
//- defines enums and constants


namespace HWProxy_ns
{

  // ============================================================================
  // Config::Config
  // ============================================================================
  HWProxy::Config::Config ()
  {
    url                 = "Not Initialised";
    periodic_timeout_ms = 100;
    ratio               = 0.001;
  }

  HWProxy::Config::Config (const Config & _src)
  {
    *this = _src;
  }
  // ============================================================================
  // Config::operator =
  // ============================================================================
  void HWProxy::Config::operator = (const Config & _src)
  {
    url                 = _src.url;
    periodic_timeout_ms = _src.periodic_timeout_ms;
    ratio               = _src.ratio;
  }

  //---------------------------------------------------------------------
  //- Ctor --------------------------------------------------------------
  //---------------------------------------------------------------------
  HWProxy::HWProxy (Tango::DeviceImpl * _host_device, 
                    const Config & _conf)
    : Tango::LogAdapter (_host_device),
    m_current (__NAN__),
    m_voltage(__NAN__),
    m_setpoint (__NAN__),
    m_cmd_index (0),
    conf (_conf),
    m_local (false)
  {
    DEBUG_STREAM << "HWProxy::HWProxy <- " << std::endl;
    //- yat::Task configure optional msg handling
    this->enable_timeout_msg (false);
    this->enable_periodic_msg (true);
    this->set_periodic_msg_period (conf.periodic_timeout_ms);

    this->m_cmd_index = CMD_RD_PS_STATUS;
  }

  //---------------------------------------------------------------------
  //- Dtor ----------------------------------------------------------------
  //---------------------------------------------------------------------
  HWProxy::~HWProxy (void)
  {
    DEBUG_STREAM << "HWProxy::~HWProxy <- " << std::endl;
  }

  //---------------------------------------------------------------------
  //- the user core of the Task ------------------------------------------
  //---------------------------------------------------------------------
  void HWProxy::handle_message (yat::Message& _msg)
    throw (yat::Exception)
  {
    //- The DeviceTask's lock_ -------------

    DEBUG_STREAM << "HWProxy::handle_message::receiving msg " << _msg.to_string() << std::endl;

    //- handle msg
    switch (_msg.type())
    {
      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message::THREAD_INIT::thread is starting up" << std::endl;

        //- "initialization" code goes here
        try
        {
          DEBUG_STREAM << " HWProxy::handle_message handling TASK_INIT try to get device proxy on  " << conf.url << std::endl;
          this->m_comm_dev  = new Tango::DeviceProxy (this->conf.url);

          //- get the identity string of the power supply
          this->get_string ("PRINT\r", this->m_identity);
          //- get the state at startup
          this->read_hard ();

        }
        catch(...)
        {
          ERROR_STREAM << " HWProxy::HWProxy exception caugth trying to get/ping device proxy on " << conf.url << std::endl;
          this->m_com_state =PS_DEVICE_PROXY_ERROR;
          return;
        }
        this->m_com_state = PS_NO_ERROR;
      } 
      break;

      //- THREAD_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling THREAD_EXIT thread is quitting" << std::endl;

        //- "release" code goes here
        if (m_comm_dev)
        {
          delete m_comm_dev;
          m_comm_dev = NULL;
        }
      }
      break;
      //- THREAD_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling THREAD_PERIODIC msg" << std::endl;
        //- code relative to the task's periodic job goes here
        //- get the HW data
        this->read_hard();
      }

      break;
      //- THREAD_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        DEBUG_STREAM << "HWProxy::handle_message handling THREAD_TIMEOUT msg" << std::endl;
      }
      break;
      //- USER_DEFINED_MSG ================
      //- WRITE_SETPOINT_MSG----------------
    case WRITE_CURRENT_MSG:
      {
        if (m_local)
          return;
         DEBUG_STREAM << "HWProxy::handle_message handling WRITE_GAIN_MSG user msg" << std::endl;
       //- get msg data...
        double * double_value = 0;
        _msg.detach_data(double_value);
        if (double_value) 
        {
          std::stringstream s;
          s << "DA 0," << std::showpos << static_cast <long> ((*double_value) / conf.ratio) << "\r";
//-std::cout << "HWProxy::WRITE_CURRENT_MSG::cmd = [" << s.str () << "]" << std::endl;
          try
          {
            this->send_command (s.str ());
            delete double_value;
          }
          catch (...)
          {
            ERROR_STREAM << "HWProxy::handle_message handling WRITE_CURRENT_MSG caught Exception" << std::endl;
            delete double_value;
          }
        }
        break;
      }
      //- OFF_MSG----------------
    case OFF_MSG:
      {
        if (m_local)
          return;
        DEBUG_STREAM << "HWProxy::handle_message handling OFF_APD_MSG user msg" << std::endl;
        this->send_command (std::string ("F\r"));
        break;
      }
      //- ON_MSG----------------
    case ON_MSG:
      {
        if (m_local)
          return;
        DEBUG_STREAM << "HWProxy::handle_message handling ON_APD_MSG user msg" << std::endl;
        this->send_command (std::string ("N\r"));
        break;
      }
      //- RESET_INTERLOCKS_MSG----------------
    case RESET_INTERLOCKS_MSG:
      {
        if (m_local)
          return;
        DEBUG_STREAM << "HWProxy::handle_message handling OFF_DISCRI_MSG user msg" << std::endl;
        this->send_command (std::string ("RS\r"));
        break;
      }
      //- REMOTE_MSG----------------
    case REMOTE_MSG:
      {
        if (m_local)
          return;
        DEBUG_STREAM << "HWProxy::handle_message handling ON_DISCRI_MSG user msg" << std::endl;
        this->send_command (std::string ("REM\r"));
        break;
      }
      //- LOCAL_MSG----------------
    case LOCAL_MSG:
      {
        if (m_local)
          return;
        DEBUG_STREAM << "HWProxy::handle_message handling OFF_GAIN_MSG user msg" << std::endl;
        this->send_command (std::string ("LOC\r"));
        break;
      }
    default:
      {
        ERROR_STREAM << "HWProxy::handle_message unhandled msg type received" << std::endl;
        break;
      }
    }

    DEBUG_STREAM << "HWProxy::handle_message message_handler : " 
      << _msg.to_string() 
      << " successfully handled" 
      << std::endl;
  }



  //---------------------------------------------------------------------
  //- State accessor ------------------------------------------------------
  //---------------------------------------------------------------------
  Tango::DevState HWProxy::get_state (void)
  { 
    DEBUG_STREAM << "HWProxy::get_state <-" << std::endl;
    Tango::DevState st = Tango::UNKNOWN;
    if (m_com_state != PS_NO_ERROR)
    {
      st = Tango::UNKNOWN;
      return st;
    }
    else if ((m_ps_state & 0x4000) != 0)
    {
      st = Tango::FAULT;
      return st;
    }
    else if ((m_ps_state & 0x800000) != 0)
    {
      st = Tango::OFF;
      return st;
    }
    else if ((m_ps_state & 0x800) != 0)
    {
      st = Tango::ON;
      return st;
    }
    else if ((m_ps_state & 0x2) != 0)
    {
      st = Tango::ALARM;
      return st;
    }
    else if ((m_ps_state & 0x8000) != 0)
    {
      st = Tango::STANDBY;
      return st;
    }
    else
    {
      st = Tango::DISABLE;
      return st;
    }
  }

  //---------------------------------------------------------------------
  //- Status accessor ------------------------------------------------------
  //---------------------------------------------------------------------
  const std::string & HWProxy::get_status (void)  
  { 
    DEBUG_STREAM << "HWProxy::get_status <-" << std::endl;
    this->m_status.clear ();
    this->m_status = this->m_identity;
    if( this->m_com_state == PS_DEVICE_PROXY_ERROR)
    {
      this->m_status = "Device Proxy Error [check device proxy name and state]\n";
      return const_cast <const std::string &> (this->m_status); 
    }
    else if( this->m_com_state == PS_UNKNOWN_ERROR)
    {
      this->m_status = "Unknown Error [call support]\n";
      return const_cast <const std::string &> (this->m_status); 
    }
    else if( this->m_com_state == PS_INITIALIZATION_ERROR)
    {
      this->m_status = "Initialization Error [check device proxy name]\n";
      return const_cast <const std::string &> (this->m_status); 
    }
    else if( this->m_com_state == PS_COMMUNICATION_ERROR)
    {
      this->m_status = "Communication Error [check device, cables, proxy name and state,...]\n";
      return const_cast <const std::string &> (this->m_status); 
    }

//-    std::cout << " HWProxy::get_status SH1 returned " << std::hex << m_ps_state << std::endl;
    
    if ((m_ps_state & 0x800000) != 0)
      this->m_status += "PS OFF\n";
    if ((m_ps_state & 0x400000) != 0)
      this->m_status += "LOCAL\n";
    else 
      this->m_status += "REMOTE\n";
    if ((m_ps_state & 0x200000) != 0)
      this->m_status += "SPARE INTERLOCK 1 (Hex 200 000)\n";
    if ((m_ps_state & 0x20000) != 0)
      this->m_status += "%\n";
    else 
      this->m_status += "AMPS AND VOLTS\n";
    if ((m_ps_state & 0x8000) != 0)
      this->m_status += "STANDBY\n";
    if ((m_ps_state & 0x4000) != 0)
      this->m_status += "SUM INTERLOCKS\n";
    if ((m_ps_state & 0x2000) != 0)
      this->m_status += "DC OVER CURRENT PROTECTION\n";
    if ((m_ps_state & 0x1000) != 0)
      this->m_status += "OVER VOLTAGE PROTECTION\n";
    if ((m_ps_state & 0x800) != 0)
      this->m_status += "PS ON\n";
    if ((m_ps_state & 0x400) != 0)
      this->m_status += "SPARE INTERLOCK 2 (Hex 400)\n";
    if ((m_ps_state & 0x200) != 0)
      this->m_status += "MAINS FAILURE\n";
    if ((m_ps_state & 0x100) != 0)
      this->m_status += "CURRENT LIMIT\n";
    if ((m_ps_state & 0x80) != 0)
      this->m_status += "EARTH LEAK FAILURE\n";
    if ((m_ps_state & 0x40) != 0)
      this->m_status += "CONVERTER OVER VOLTAGE\n";
    if ((m_ps_state & 0x20) != 0)
      this->m_status += "MPS OVERTEMPERATURE\n";
    if ((m_ps_state & 0x4) != 0)
      this->m_status += "SPARE INTERLOCK 3 (Hex 4)\n";
    if ((m_ps_state & 0x2) != 0)
      this->m_status += "MPS NOT READY\n";
    if ((m_ps_state & 0x1) != 0)
      this->m_status += "MPS FAN FAULT\n";

    return const_cast <const std::string &> (this->m_status); 
  }


  //---------------------------------------------------------------------
  //- read_hard --------------------------------------------------------------
  //---------------------------------------------------------------------
	void HWProxy::read_hard (void)
	{
    //- do not throw exceptions till this is called by the periodic task
    //- so they are catched and ignored by the task in order to continue
    DEBUG_STREAM << "HWProxy::read_hard for cmd " << polled_cmds_str [m_cmd_index] << " <- " << std::endl;
    
    try
    {

      if (this->m_cmd_index >= CMD_MAX_INDEX)
        this->m_cmd_index = CMD_RD_PS_STATUS;

      switch (m_cmd_index)
      {
        //- power supply channel 1-------------------------------------------
        //- PS status 
        case CMD_RD_PS_STATUS : 
        {
          this->get_process_state ();
          break;
        }
        //- current
        case CMD_RD_CURRENT :
        {
          this->m_current = get_process_value_l (polled_cmds_str [1]);
          this->m_current *= conf.ratio;
          break;
        }
        //- voltage
        case CMD_RD_VOLTAGE :
        {
          this->m_voltage = get_process_value_l (polled_cmds_str [2]);
          this->m_voltage *= conf.ratio;
          break;
        }
        //- setpoint
        case CMD_RD_SETPOINT :
        {
          this->m_setpoint = get_process_value_l (polled_cmds_str [3]);
          this->m_setpoint *= conf.ratio;
          break;
        }
      }
      ++ m_cmd_index;
    }
    catch (Tango::DevFailed &e)
    {
      m_com_state = PS_COMMUNICATION_ERROR;
      ERROR_STREAM << "HWProxy::read_hard caught DevFailed [" << e.errors[0].desc << "]" << std::endl;
    }
    catch (...)
    {
      m_com_state = PS_COMMUNICATION_ERROR;
      ERROR_STREAM << "HWProxy::read_hard caught (...)" << std::endl;
    }
  }


  //-------------------------------------------------------------------------------------------
  //- read value on the Hard, returns a double value  (process values)
  //-------------------------------------------------------------------------------------------
  double HWProxy::get_process_value_d (const std::string & cmd)
  {
    DEBUG_STREAM << "HWProxy::get_process_value_d <- " << std::endl;
    if( this->m_com_state == PS_DEVICE_PROXY_ERROR)
      return __NAN__;

     Tango::DevString ds;
    ds = CORBA::string_dup (cmd.c_str ());

    std::string resp;
    double tmp = __NAN__;
    Tango::DeviceData ddinflush, ddinwrite, ddout;
    ddinflush << Tango::DevLong (2);
    ddinwrite << ds;
    try
    {
      m_comm_dev->command_inout ("DevSerFlush", ddinflush);
      m_comm_dev->command_inout ("DevSerWriteString", ddinwrite);
      ddout = m_comm_dev->command_inout ("DevSerReadLine");
      ddout >> resp;
      DEBUG_STREAM << "HWProxy::get_process_value_d cmd [" << cmd << "] response [" << resp << "]" << std::endl;
      if(resp.length() > 0)
      {
        try
        {
          tmp = yat::XString <double>::to_num (resp);
        }
        catch (yat::Exception)
        {
          ERROR_STREAM << "HWProxy::get_process_value_d catched YAT exception trying to convert" << resp << std::endl;
          m_ps_status += "could not obtain value for command " + cmd;
        }
        m_com_state = PS_NO_ERROR;
      }
      else
      {
        ERROR_STREAM << "HWProxy::get_process_value_d did not receive response to cmd " << cmd << std::endl;
        m_com_state = PS_COMMUNICATION_ERROR;
      }
      return tmp;
    }
    catch (yat::Exception & ye)
    {
      ERROR_STREAM << "HWProxy::get_process_value_d catched YAT exception desc [" << ye.errors[0].desc << "]trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::get_process_value_d catched Tango::DevFailed desc [" << e.errors[0].desc << "] trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_process_value_d catched exception trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
  }  
  //-------------------------------------------------------------------------------------------
  //- read value on the Hard, returns a long value  (process values)
  //-------------------------------------------------------------------------------------------
  long HWProxy::get_process_value_l (const std::string & cmd)
  {
    DEBUG_STREAM << "HWProxy::get_process_value <- " << std::endl;
    if( this->m_com_state == PS_DEVICE_PROXY_ERROR)
      return -1;

    Tango::DevString ds;
    ds = CORBA::string_dup (cmd.c_str ());
    std::string resp;
    long tmp;
    Tango::DeviceData ddinflush, ddinwrite, ddout;
    ddinflush << Tango::DevLong (2);
    ddinwrite << ds;
    try
    {
      m_comm_dev->command_inout ("DevSerFlush", ddinflush);
      m_comm_dev->command_inout ("DevSerWriteString", ddinwrite);
      ddout = m_comm_dev->command_inout ("DevSerReadLine");
      ddout >> resp;
      DEBUG_STREAM << "HWProxy::get_process_value_l cmd [" << cmd << "] response [" << resp << "]" << std::endl;
      if(resp.length() > 0)
      {
        tmp = yat::XString <long>::to_num (resp);
        m_com_state = PS_NO_ERROR;
      }
      else
      {
        ERROR_STREAM << "HWProxy::get_process_value_l did not receive response to cmd " << cmd << std::endl;
        m_com_state = PS_COMMUNICATION_ERROR;
      }
      return tmp;
    }
    catch (yat::Exception & ye)
    {
      ERROR_STREAM << "HWProxy::get_process_value_l catched YAT exception desc [" << ye.errors[0].desc << "]trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::get_process_value_l catched Tango::DevFailed desc [" << e.errors[0].desc << "] trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_process_value_l catched (...) exception trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
  }

  //-------------------------------------------------------------------------------------------
  //- get PS State
  //-------------------------------------------------------------------------------------------
  void HWProxy::get_process_state (void)
  {
    DEBUG_STREAM << "HWProxy::get_process_state <- " << std::endl;
    if( this->m_com_state == PS_DEVICE_PROXY_ERROR)
      return;

    std::string resp;
    Tango::DeviceData ddinflush, ddinwrite, ddout;
    ddinflush << Tango::DevLong (2);
    Tango::DevString ds;
    ds = CORBA::string_dup ("S1H\r");
    ddinwrite << ds;
    try
    {
      m_comm_dev->command_inout ("DevSerFlush", ddinflush);
      m_comm_dev->command_inout ("DevSerWriteString", ddinwrite);
      ddout = m_comm_dev->command_inout ("DevSerReadLine");
      ddout >> resp;
      DEBUG_STREAM << "HWProxy::get_process_value_l cmd [SH1\r] response [" << resp << "]" << std::endl;
      if(resp.length() > 0)
      {
        std::stringstream ss;
        ss << std::hex << resp;
        ss >> this->m_ps_state;
//-        std::cout << "get_process_state value = " << std::hex << m_ps_state << std::endl;
      }
      else
      {
        ERROR_STREAM << "HWProxy::get_process_state did not receive response to cmd SH1" << std::endl;
        m_com_state = PS_COMMUNICATION_ERROR;
      }
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_process_state caught (...) trying to send SH1\r" << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
  }
  //-------------------------------------------------------------------------------------------
  //- sends a command on the Hard, returns no value
  //-------------------------------------------------------------------------------------------
  void HWProxy::send_command (const std::string & cmd)
  {
    DEBUG_STREAM << "HWProxy::send_command <- [" << cmd << "]" << std::endl;
//-    std::cout << "HWProxy::send_command <- [" << cmd << "]" << std::endl;
    if( this->m_com_state == PS_DEVICE_PROXY_ERROR)
      return;
    Tango::DevString ds;
    ds = CORBA::string_dup (cmd.c_str ());
    Tango::DeviceData ddinflush, ddinwrite;
    ddinflush << Tango::DevLong (2);
    ddinwrite << ds;
    try
    {
      m_comm_dev->command_inout ("DevSerFlush", ddinflush);
      m_comm_dev->command_inout ("DevSerWriteString", ddinwrite);
      m_com_state =PS_NO_ERROR;
    }
    catch (yat::Exception & ye)
    {
      ERROR_STREAM << "HWProxy::send_command catched YAT exception desc [" << ye.errors[0].desc << "]trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::send_command catched Tango::DevFailed desc [" << e.errors[0].desc << "] trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::send_command catched (...) exception trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
  }
 //-------------------------------------------------------------------------------------------
  //- read value on the Hard, returns the full string (status and version)
  //-------------------------------------------------------------------------------------------
  void HWProxy::get_string (const std::string & cmd, std::string & response)
  {
    DEBUG_STREAM << "HWProxy::get_string <- " << std::endl;
    if( this->m_com_state == PS_DEVICE_PROXY_ERROR)
      return;
    Tango::DeviceData ddinflush, ddinwrite, ddout;
    ddinflush << Tango::DevLong (2);
    ddinwrite << cmd.c_str ();
    try
    {
      m_comm_dev->command_inout ("DevSerFlush", ddinflush);
      m_comm_dev->command_inout ("DevSerWriteString", ddinwrite);
      ddout = m_comm_dev->command_inout ("DevSerReadLine");
      ddout >> response;
      m_com_state =PS_NO_ERROR;
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_string catched exception trying to send " << cmd << std::endl;
      m_com_state = PS_COMMUNICATION_ERROR;
    }
  }

} //- namespace

