static const char *RcsId = "$Id $";
//+=============================================================================
//
// file :         PSDanfysikS7000StateMachine.cpp
//
// description :  C++ source for the PSDanfysikS7000 and its alowed 
//                methods for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
// $Date: 2011-03-21 15:59:08 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/PowerSupply/PSDanFysikS7000/src/PSDanfysikS7000StateMachine.cpp,v $
// $Log: not supported by cvs2svn $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <tango.h>
#include <PSDanfysikS7000.h>
#include <PSDanfysikS7000Class.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace PSDanfysikS7000_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_current_allowed
// 
// description : 	Read/Write allowed for current attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_current_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_setpoint_allowed
// 
// description : 	Read/Write allowed for setpoint attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_setpoint_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_powerSupplyStatus_allowed
// 
// description : 	Read/Write allowed for powerSupplyStatus attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_powerSupplyStatus_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_isOn_allowed
// 
// description : 	Read/Write allowed for isOn attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_isOn_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_isAnalog_allowed
// 
// description : 	Read/Write allowed for isAnalog attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_isAnalog_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_isFault_allowed
// 
// description : 	Read/Write allowed for isFault attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_isFault_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_voltage_allowed
// 
// description : 	Read/Write allowed for voltage attribute.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_voltage_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_On_allowed
// 
// description : 	Execution allowed for On command.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_On_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_Off_allowed
// 
// description : 	Execution allowed for Off command.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_Off_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_Reset_allowed
// 
// description : 	Execution allowed for Reset command.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_Reset_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_Analog_allowed
// 
// description : 	Execution allowed for Analog command.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_Analog_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		PSDanfysikS7000::is_Profibus_allowed
// 
// description : 	Execution allowed for Profibus command.
//
//-----------------------------------------------------------------------------
bool PSDanfysikS7000::is_Profibus_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

}	// namespace PSDanfysikS7000_ns
