import os
from conan import ConanFile
from conan.tools.build import can_run


class PSDanFysikS7000TestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        if can_run(self):
            if self.settings.os == "Windows":
                self.run("ds_PSDanFysikS7000 2>&1 | findstr \"usage\"", env="conanrun")
            else:
                self.run("ds_PSDanFysikS7000 2>&1 | grep \"usage\"", env="conanrun")
